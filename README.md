## Tooling

This repo will contain central config and build tooling used across multiple golang projects.
This includes:

- gitlab ci config
- taskfiles
- golangci-lint

The aim of this repository is to make it easier to keep my configuration and tooling in sync across my various 
golang projects. Including libraries, CLI tools and webservices.
